#include <QGuiApplication>
#include <QQmlApplicationEngine>
// *** ADDED BY HEADER FIXUP ***
#include <istream>
// *** END ***
#include <iostream>
#include <string>
#include "functions2.h"
#include "Stop.h"
#include <fstream>
#include <vector>
#include <sstream>
#include "Network.h"
#include "Journey.h"
#include "JourneyN.h"
#include "Time_mh.h"
#include "ConnectionN.h"
#include <algorithm>
#include <QQuickView>
#include <QQmlContext>
#include <journeylist.h>
#include <QQuickStyle>



int main(int argc, char *argv[])
{
#if QT_VERSION < QT_VERSION_CHECK(6, 0, 0)
    QCoreApplication::setAttribute(Qt::AA_EnableHighDpiScaling);
#endif
    QGuiApplication app(argc, argv);
    app.setOrganizationName("František Prinz");
    app.setOrganizationDomain("Public Transport Searcher");
    app.setWindowIcon(QIcon(":/ikon.png"));
    Network network;
    /*network.loadfromstring("timetables/loadfile.txt");
    Time_mh t ("14:00");
    cout << network.lines[network.lines.size()-1]->number << endl;
    std::string a = "Poliklinika";
    Stop* stop = network.get_stop(a);
    cout << network.lines[network.lines.size()-1]->depart(t, a) << endl;
    network.find_journeys(network.get_stop((std::string) "Jana Palacha"), stop, t);*/
    vector<Line*> a;
   QQmlApplicationEngine engine;
    QQuickStyle::setStyle("Universal");
    const QUrl url(QStringLiteral("qrc:/main.qml"));
    QQmlContext* context = engine.rootContext();
    JourneyList jnl;
    //std::cout << typeid(jnl).name() << std::endl;
    context->setContextProperty("_network", &network);
    context->setContextProperty("_journeylist", &jnl);
    engine.rootContext()->setContextProperty("_network", &network);
    engine.rootContext()->setContextProperty("_journeylist", &jnl);
    QObject::connect(&engine, &QQmlApplicationEngine::objectCreated,
                     &app, [url](QObject *obj, const QUrl &objUrl) {
        if (!obj && url == objUrl)
            QCoreApplication::exit(-1);
    }, Qt::QueuedConnection);

   engine.load(url);
     app.exec();
     return 0;
}







