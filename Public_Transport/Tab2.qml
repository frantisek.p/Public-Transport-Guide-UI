import QtQuick 2.12
import QtQuick.Window 2.12
import QtQuick.Controls 2.12
import QtQuick.Layouts 1.12
import QtQuick.Dialogs 1.2

Rectangle{
    id: wind
    anchors. fill: parent
    implicitHeight: 600
    implicitWidth: 640
    //visible: bar.currentIndex ===0? true : false

    //implicitHeight: 780

    //property var con: _network.qjourneys_no
    //property int hod: sbmin.value
   // proper
    //property string str: object.read
    property alias cb1_index: linka.currentIndex
    property string cislo_linky: "a"
    property string cislo_trasy: "b"
    property bool line_displayed: false



    Rectangle{
        id: rect
        x: wind.width/16
        implicitHeight: parent.height/10.*4.
        implicitWidth: wind.width/8*7
        anchors.right: parent.right
        anchors.left: parent.left
        anchors.margins: 10
        radius: 5


        color: "lightblue"





        ColumnLayout{
            id: cl
            //spacing: 50
            width: parent.width
            anchors.top: parent.top
            anchors.left: parent.left
            anchors.margins: 20
            anchors.fill: parent


            RowLayout{
                id: r1
                Layout.alignment: Qt.AlignTop


                Text{
                    id: r1t1
                    text: "Vyhledávač jízdních řádů MHD Břeclav"
                    font.pointSize: 16
                    font.bold: true
                }
            }

            RowLayout{
                id: r11
                Layout.alignment: Qt.AlignTop
                //Layout.alignment: Qt.AlignTop

                Text{
                    id: r1t11
                    text: "Výpis seznamu zastávek obsluhovaných vybranou linkou a trasou"
                    font.pointSize: 12
                    font.bold: false
                }
            }







            RowLayout{
                id: r_cislo_linky
                implicitWidth: parent.width
                Layout.alignment: Qt.AlignLeft
                Layout.fillWidth: true

                Text{
                    text: "Číslo linky:"

                }
                ComboBox{
                    id: linka
                    editable: false
                    Layout.fillWidth: true
                    model: _network.qlines

                }

            }

            RowLayout{
                id: r_cislo_trasy
                implicitWidth: parent.width
                Layout.alignment: Qt.AlignLeft
                Layout.fillWidth: true

                Text{
                    text: "Číslo trasy:"


                }
                ComboBox{
                    id: trasa
                    editable: false
                    Layout.fillWidth: true
                    model: _network.routes_in_line[cb1_index]

                }

            }

            RowLayout{
                implicitWidth: parent.width
                Layout.alignment: Qt.AlignCenter
                Layout.fillWidth: true

                /*Text{

                    Layout.fillWidth: true
                }*/

                Button{
                    id: bt
                    text: "Vyhledat"
                    //Layout.horizontalCenter: Qt.AlignHCenter
                    onClicked: {
                        linka_trasa.text = "Linka č. " + linka.currentText + " trasa č. " + trasa.currentText
                        pocatecni_zast.text = "Počáteční zastávka"
                        _network.create_qlinestops(trasa.currentText)
                        cilova_zast.text = "Cílová zastávka"
                        scrollView.vScrollBar.setPosition(0)
                    }
                    //enabled: (r2.cb_num == r3.cb_num ? false : true)

                }

                Button{
                    id:bt_quit
                    text: "Konec"
                    onClicked: Qt.quit();
                }

                    }

            }




        }

    Rectangle
    {
        implicitHeight: wind.height- rect.height
        width: parent.width
        id: rect2
        anchors.margins: 10
        anchors.topMargin: 0
        anchors.top: rect.bottom
        anchors.left: parent.left
        anchors.right: parent.right

        //implicitHeight: parent.implicitHeight - rect.implicitHeight - autor.height /* - cpr.implicitHeight*/
        color: "lightyellow"
        radius: 5

    ScrollView {
        //property alias a: ScrollBar.vertical.
        property ScrollBar vScrollBar: ScrollBar.vertical
        id: scrollView
        width: parent.width
        height: parent.height
        //availableHeight: parent.height
        //anchors.margins: 15
        anchors.fill: parent
        contentWidth: parent.width

        contentHeight: children.height
        //contentHeight: parent.height
        clip: true
        //ScrollBar.horizontal: false
        //ScrollBar.horizontal.policy: ScrollBar.AlwaysOff
        ScrollBar.vertical.policy: ScrollBar.AlwaysOn
        //Text.wrapMode: Text.WrapAnywhere

        ColumnLayout {
                id: inr
               // anchors.fill: parent
                width: parent.width
                layoutDirection: "LeftToRight"
                anchors.left: parent.left
                anchors.margins: 15
                anchors.rightMargin: 30
                //anchors.horizontalCenter: parent
                anchors.top: parent.top
                anchors.right: parent.right
                anchors.bottom: parent.bottom

        RowLayout {
                id: rl1
                //Layout.preferredHeight: 75
                Layout.alignment: Qt.AlignHCenter
                Layout.fillWidth: true

                Text{
                id: linka_trasa
                font.pointSize: 14
                Layout.alignment:  Qt.AlignVCenter
                font.bold: true
                text: ""
            }}
        RowLayout {
                id: rl2
                //Layout.preferredHeight: 75
                Layout.alignment: Qt.AlignHCenter
                Layout.fillWidth: true

                Text{
                id: pocatecni_zast
                font.pointSize: 12
                Layout.alignment:  Qt.AlignVCenter
                color: "red"
                text: ""
            }}

        Repeater{
            model: _network.qlinestops.length


      RowLayout {
                        Layout.alignment: Qt.AlignHCenter
                        Layout.fillWidth: true
                Text{
                    //id: spojeni_neex
                    text: _network.qlinestops[index]
                    font.pointSize: 12
                }
            }
            }
        RowLayout {
                id: rllast
                //Layout.preferredHeight: 75
                Layout.alignment: Qt.AlignHCenter
                Layout.fillWidth: true

                Text{
                id: cilova_zast
                font.pointSize: 12
                Layout.alignment:  Qt.AlignVCenter
                color: "red"
                text: ""
            }}

        RowLayout {
            Layout.minimumHeight: 20

        }

         }}
    }


               }

