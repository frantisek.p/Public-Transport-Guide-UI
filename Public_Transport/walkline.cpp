#include "walkline.h"

WalkLine::WalkLine()
{
        type = "Pěší přesun";
}

Time_mh WalkLine::depart(Time_mh &time, string &stop)
{

    int index = find_stop(stop);
    /*for(unsigned int i=0;i<routes.size();i++)
    {
     if((time<=(this->routes[i])->dep[index]))

           {
               //cout << "depart if" << endl;
               return (routes[i]->dep)[index];
           }
    }*/
    Time_mh t("99:22");
    return time;
}

Time_mh WalkLine::arrive(int route_num, Stop *s)
{
    Time_mh t("99:00");
    return t;
}

Time_mh WalkLine::arrive(Route* route, Stop *s)
{
    Time_mh t("99:00");
    if(this->StopList[0]->Name == s->Name)
        return route->dep[0];
    if(this->StopList[1]->Name == s->Name)
        return route->dep[1];
    return t;
}

Route* WalkLine::get_route(Time_mh &time, Stop &stop)
{
    Route *r = new Route();
    int index = find_stop(stop.Name);
    r->dep.reserve(2);
    if(index==0)
    {
        (r->dep[0])=time;
        (r->dep[1])= time + walk_time;
    }
    if(index==1)
    {
        r->dep[0]=time - walk_time;
        r->dep[1]= time;
    }
    return r;
}

WalkLine::WalkLine(int _number, vector <Stop*> _StopList, int _shield_number)
{
    number = _number;
    StopList = _StopList;
    shield_number = _shield_number;
    type = "Pesi";
}
