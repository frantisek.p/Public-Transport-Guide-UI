import QtQuick 2.12
import QtQuick.Window 2.12
import QtQuick.Controls 2.12
import QtQuick.Layouts 1.12
import QtQuick.Dialogs 1.2

Rectangle{
    id: wind
    anchors. fill: parent
    implicitHeight: 600
    implicitWidth: 640
    //visible: bar.currentIndex ===0? true : false

    //implicitHeight: 780

    //property var con: _network.qjourneys_no
    //property int hod: sbmin.value
   // proper
    //property string str: object.read
    property alias a: windd
    property alias odjezdova_zastavka: r2.cb_text
    property alias prijezova_zastavka: r3.cb_text
    property alias hodin: sbhod.value
    property alias minut: sbmin.value



    Rectangle{
        id: rect
        x: wind.width/16
        implicitHeight: parent.height/10.*4.
        implicitWidth: wind.width/8*7
        anchors.right: parent.right
        anchors.left: parent.left
        anchors.margins: 10
        radius: 5


        color: "lightblue"





        ColumnLayout{
            id: cl
            //spacing: 50
            width: parent.width
            anchors.top: parent.top
            anchors.left: parent.left
            anchors.margins: 20
            anchors.fill: parent


            RowLayout{
                id: r1
                Layout.alignment: Qt.AlignTop
                //Layout.alignment: Qt.AlignTop

                Text{
                    id: r1t1
                    text: "Vyhledávač jízdních řádů MHD Břeclav"
                    font.pointSize: 16
                    font.bold: true
                }
            }

            RowLayout{
                id: r11
                Layout.alignment: Qt.AlignTop
                //Layout.alignment: Qt.AlignTop

                Text{
                    id: r1t11
                    text: "Návrh jízdních řádů varianta 2021/2.1. - pracovní dny"
                    font.pointSize: 12
                    font.bold: false
                }
            }

            Line{

                id: r2
                tt2: oz
                wwidth: sbhod.width+sbmin.width + 2*rsp.spacing+ dvojtecka.width
                //a: lm
                Layout.alignment: Qt.AlignLeft
                //anchors.top: r1.bottom

            }

            Line{

                id: r3
                tt2: pz
                wwidth: sbhod.width+sbmin.width + 2*rsp.spacing+ dvojtecka.width
                //a: lm
                Layout.alignment: Qt.AlignLeft
            }





            RowLayout{
                id: rsp
                implicitWidth: parent.width
                Layout.alignment: Qt.AlignLeft
                Layout.fillWidth: true

                Text{
                    text: co
                    Layout.fillWidth: true

                }
                SpinBox{
                    id: sbhod
                    from: 0
                    value: 12
                    to:23
                    editable: true

                }
                Text{
                    id: dvojtecka
                    text: ":"
                    width: 100
                }
                SpinBox{
                    property int factor: 5
                    id: sbmin
                    Layout.alignment: Qt.AlignRight
                    from: 00
                    to: 55
                    stepSize: 5
                    editable: true

                    textFromValue: function(value, locale)
                    { return Number(Math.floor(value/factor)*factor).toLocaleString(locale, 'f', 0); }
                    valueFromText: function(locale) {
                        return parseFloat((locale/factor)*factor);
                    }
                    //Layout.anchors.right: parent.right
                }
            }

            RowLayout{
                implicitWidth: parent.width
                Layout.alignment: Qt.AlignCenter
                Layout.fillWidth: true

                /*Text{

                    Layout.fillWidth: true
                }*/

                Button{
                    id: bt
                    text: "Vyhledat"
                    //Layout.horizontalCenter: Qt.AlignHCenter
                    onClicked: {
                        rl1t1.text = "Spojeni z " + r2.cb_text + " do " + r3.cb_text
                        _network.vyhledat(r2.cb_num, r3.cb_num, sbhod.value, sbmin.value )
                        _network.qqjourneyslist.length === 0 ? spojeni_neex.text="Hledané spojení neexistuje!" : spojeni_neex.text=""
                        scrollView.vScrollBar.setPosition(0)
                        bt_save.enabled = true
                    }
                    enabled: (r2.cb_num == r3.cb_num ? false : true)

                }

                Button{
                    id:bt_quit
                    text: "Konec"
                    onClicked: Qt.quit();
                }

                Button{
                    id:bt_change
                    text: "Prohod zastavky"
                    onClicked: {
                        var num1 = r2.cb_num;
                        r2.change_displayed_number(r3.cb_num);
                        r3.change_displayed_number(num1);
                        console.log(currentDate.toLocaleDateString(Qt.locale(), "ddd"));
                    }
                }

                    Button {
                        id: bt_save
                        text: "Ulož spoje"
                        enabled: false
                        onClicked:{
                            fd.open();

                        }

                    }

            }




           /* Line{

                id: r4
                tt2: co
                Layout.alignment: Qt.AlignLeft
            }*/


        }
    }
    Rectangle
    {
        implicitHeight: wind.height- rect.height
        width: parent.width
        id: rect2
        anchors.margins: 10
        anchors.topMargin: 0
        anchors.top: rect.bottom
        anchors.left: parent.left
        anchors.right: parent.right
        //anchors.bottom: autor.top

        //implicitHeight: parent.implicitHeight - rect.implicitHeight - autor.height /* - cpr.implicitHeight*/
        color: "lightyellow"
        radius: 5

    ScrollView {
        //property alias a: ScrollBar.vertical.
        property ScrollBar vScrollBar: ScrollBar.vertical
        id: scrollView
        width: parent.width
        height: parent.height
        //availableHeight: parent.height
        //anchors.margins: 15
        anchors.fill: parent
        contentWidth: parent.width

        contentHeight: children.height
        //contentHeight: parent.height
        clip: true
        //ScrollBar.horizontal: false
        //ScrollBar.horizontal.policy: ScrollBar.AlwaysOff
        ScrollBar.vertical.policy: ScrollBar.AlwaysOn
        //Text.wrapMode: Text.WrapAnywhere

        ColumnLayout {
                id: inr
               // anchors.fill: parent
                width: parent.width
                layoutDirection: "LeftToRight"
                anchors.left: parent.left
                anchors.margins: 15
                anchors.rightMargin: 30
                //anchors.horizontalCenter: parent
                anchors.top: parent.top
                anchors.right: parent.right
                anchors.bottom: parent.bottom

        RowLayout {
                id: rl1
                Layout.preferredHeight: 75
                Layout.alignment: Qt.AlignHCenter
                Layout.fillWidth: true

                Text{
                id: rl1t1
                font.pointSize: 16
                Layout.alignment:  Qt.AlignVCenter
                font.bold: false
                font.underline: true
                text: ""
            }}

      RowLayout {
                        id: rl2
                        Layout.preferredHeight: _network.qqjourneyslist.length===0 ? 75:0
                        Layout.alignment: Qt.AlignHCenter
                        Layout.fillWidth: true
                Text{
                    id: spojeni_neex
                    text:""
                    font.pointSize: 16
                    color: "red"
                    font.bold: true
                }
            }

        Repeater{
            model: _network.qqjourneyslist.length
        ColumnLayout{
            //parent: inr
            Layout.preferredWidth: wind2.width
            Layout.maximumWidth: wind2.width
            anchors.margins: 15
            //anchors.horizontalCenter: parent
            //anchors.top: inr.top

            //anchors.bottom: parent.bottom

       Repeater{
           id: rep1
            model: _network.qqjourneyslist[index]
                //width: inr.width
                //height: 50
            ColumnLayout {
                //parent: inr
                Layout.fillWidth: true
                anchors.margins: 15
                Layout.maximumWidth: wind2.width
                //anchors.horizontalCenter: parent
                //anchors.top: inr.top
                //anchors.right: inr.right

                RowLayout{
                   // id: rr1
                    //height: 20
                    Layout.preferredHeight: 20
                    spacing: 10
                    Layout.maximumWidth: wind2.width

                    // parent: inr
                    //anchors.bottom: rr2.top

                    Text{
                       // id:t3
                        //width: 100
                    text: model.type
                    font.pointSize: 12
                    color: "black"
                    Layout.alignment: Qt.AlignLeft
                    verticalAlignment: Text.AlignBottom
                    //Layout.preferredWidth: parent.implicitWidth/3
                    //Layout.fillWidth: true
                    }

                    Text{
                       // id:t3
                        //width: 100
                    text: model.lineno
                    font.pointSize: 12
                    color: "red"
                    Layout.alignment: Qt.AlignLeft
                    verticalAlignment: Text.AlignBottom
                    //Layout.preferredWidth: parent.implicitWidth/3
                    //Layout.fillWidth: true
                    }

                Item{
                Layout.fillWidth: true
                }
                }

                RowLayout{
                    id: rr2
                    //height: 20
                    Layout.preferredHeight: 35
                    Layout.alignment: Qt.AlignBottom
                    spacing: 10
                    // parent: inr
                        Text{
                            id:t1
                            //width: wind.Width/3.
                            //anchors.left: parent.left
                            //anchors.top: parent.top
                        text: model.startstop
                        font.pointSize: 12
                        Layout.minimumWidth: wind.implicitWidth/3
                        verticalAlignment: Text.AlignVCenter

                        }



                Text{
                    id:t2
                    verticalAlignment: Text.AlignVCenter
                    //width: 100

                text: model.starttime
                font.pointSize: 12
                font.bold: true
                //Layout.fillWidth: true
                //anchors.left: t1.right
                }

               Image{
                    fillMode: Image.PreserveAspectFit
                  /* border.right: 50
                   horizontalTileMode: BorderImage.Stretch
                   verticalTileMode: BorderImage.Stretch*/


                    source: "qrc:/red_arrow.png"
                    Layout.preferredHeight: rr2.height/2.
                    Layout.maximumHeight:  rr2.height/2.
                    Layout.fillWidth: true
                }

                Text{
                    x:wind.implicitWidth/12*9
                    id: t4
                    verticalAlignment: Text.AlignVCenter

                text: model.endstop
                Layout.alignment: Qt.AlignLeft
                font.pointSize: 12
                Layout.minimumWidth: wind.implicitWidth/3
                //anchors.left: t3.right
                }
                Text{
                    id: t5
                    //width: 100
                text: model.endtime
                Layout.alignment: Qt.AlignRight
                font.pointSize: 12
                font.bold: true
                //Layout.fillWidth: true
                //anchors.left: t4.right
                }
                }
       }}
                RowLayout{
                    //height: 12
                    // parent: inr
                    Layout.preferredHeight: 50
                    //anchors.top: rep1.bottom


                    Rectangle{
                        //width: parent.width
                        //Layout.
                        //Layout.row: 3*index
                        Layout.preferredHeight: 12
                        Layout.fillWidth: true
                        Layout.alignment: Qt.AlignTop
                        color: "grey"
                        //Layout.alignment: Qt.AlignLeft
                        //Layout.fillWidth: parent
                        //Layout.alignment: Qt.Right
                        radius: 5
                    }
                     }
       }
}
         }}
    }


                Rectangle {
                   id: windd
                   anchors. fill: parent
                   implicitHeight: 600
                   implicitWidth: 640
                   color: "white"
                   visible: true

               /*    Image{
                       source: "qrc:/ikon.png"
                       //fillMode: Image.PreserveAspectFit
                       width: parent.width
                       height: parent.height*/




               ProgressBar{

                   id: pb
                   anchors.fill: parent
                   anchors.margins: 15
                   height: parent.implicitHeight/3
                   width: parent.implicitWidth/4.*3
                   value: qloader_state
                    indeterminate: true
                    //visualPosition: value
                   property double qloader_state: _network.loader_state

               }
               Text {
                   id: t
                   text: qsTr("Aplication is loading...")
                   color: "red"
                   y: wind2.height/3
                   font.pointSize: 30
                   //anchors.top: pb.bottom
                   anchors.horizontalCenter: pb.horizontalCenter

               }
                   //}


               }

               }

