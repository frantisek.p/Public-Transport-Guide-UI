// *** ADDED BY HEADER FIXUP ***
#include <istream>
// *** END ***
#ifndef NETWORK_H
#define NETWORK_H
#include "Line.h"
#include <vector>
#include <string>
#include <fstream>
#include <sstream>
#include <iostream>
#include <Connection.h>
#include <ConnectionN.h>
#include <Journey.h>
#include <JourneyN.h>
#include <QObject>
#include <QList>
#include <QFile>
#include <QUrl>
#include <QTextStream>
//#include <object.h>
#include <journeylist.h>
#include <walkline.h>


// TODO Simplify the searching engine and add more than 4 connections


class Network : public QObject
{
    Q_OBJECT
    public:
        Time_mh time;
        QString folder_name;
        Q_PROPERTY (QString folder_name READ get_folder_name NOTIFY folder_name_changed);
        Q_PROPERTY (QList <int> qjourneys_no READ get_qjourneys_no NOTIFY qjourneys_no_changed)
        Q_PROPERTY (QList <JourneyList*> qqjourneyslist READ get_qqjourneyslist NOTIFY qqjourneyslist_changed)
        vector<int> journeys_no = {10}; //stores in i how many journeys in journeys[i]
        Stop* from;
        Stop* to;
        vector<JourneyN*> journeys;
        vector<JourneyN*> journeys_output;
        QStringList qst;
        Q_INVOKABLE void vyhledat(int from, int to, int hod, int min);
        Q_PROPERTY(QStringList qstops READ get_qstops NOTIFY qstops_changed);
        Q_PROPERTY(QStringList qqst READ get_qqst NOTIFY qqst_changed);
        Q_PROPERTY(float loader_state READ get_loader_state WRITE set_loader_state NOTIFY loader_state_changed);
        Q_INVOKABLE void save_journeis_to_file( QUrl source , const QString from, const QString to, int hod, int min);
        void load();
        void loadfromstring(std::string filename);
        Q_INVOKABLE void load(QString filename2);
        vector <Line*> lines;
        Q_PROPERTY(QStringList qlines READ get_qlines NOTIFY qlines_changed);
        Q_PROPERTY(QList <QStringList> routes_in_line READ get_routes_in_line NOTIFY routes_in_line_changed);
        Q_INVOKABLE void create_qlinestops(int line_number);
        Q_PROPERTY(QStringList qlinestops READ get_qlinestops NOTIFY qlinestops_changed);
        Q_INVOKABLE void set_folder_name();


        //all stops
        vector <Stop*> stops;
        //describes the stops where for changing
        vector <Stop*> change_stops;
        //store Connections from first coordinate Stop i from StopList to Stop j from StopList
        vector<vector<vector<ConnectionN*>>> connection_list;
        vector<vector<vector<ConnectionN*>>> connection2_list;
        vector<vector<vector<ConnectionN*>>> connection3_list;
        vector<vector<vector<ConnectionN*>>> connection4_list;

        Network();
        virtual ~Network();
        Network(const Network& other);

        Network& operator=(const Network& other);

        void read_busline(string name);

        Stop* get_stop(int Id);
        Stop* get_stop(string name);

        Line* get_line (int number);

        vector<ConnectionN*>& get_connection_from_table(Stop* start, Stop* endd);
        vector<ConnectionN*>& get_2connection_from_table(Stop* start, Stop* endd);
        vector<ConnectionN*>& get_3connection_from_table(Stop* start, Stop* endd);
        vector<ConnectionN*>& get_4connection_from_table(Stop* start, Stop* endd);

        vector<JourneyN*> find_journeys (Stop* start, Stop* endd, Time_mh depart);

        QStringList get_qstops();

        QStringList get_qqst();

        QList <int> get_qjourneys_no();

        QStringList m_qlines;

        QStringList m_qlinestops;

        QList <JourneyList*> qjourneyslist;

        QList <JourneyList*> get_qqjourneyslist()
        {
            return qjourneyslist;
        }

        void create_journeys(int i_max);

        void create_qjourneyslist();

        float get_loader_state()
        {
            return m_loader_state;
        }

        void set_loader_state(float loader_state)
        {
            m_loader_state = loader_state;
        }

        float m_loader_state= 0.0;

        QStringList get_qlines() const
        {
            return m_qlines;
        }

        QList <QStringList> get_routes_in_line() const
        {
            return m_routes_in_line;
        }

        QList <QStringList> m_routes_in_line;

        QStringList get_qlinestops() const
        {
            return m_qlinestops;
        }

        QString get_folder_name() const
        {
            return folder_name;
        }

signals:
        void qstops_changed();
        void qqst_changed();
        void a_changed();
        void qjourneys_no_changed();
        void qqjourneyslist_changed();
        void loader_state_changed();
        void qlines_changed();
        void routes_in_line_changed();        
        void qlinestops_changed();
        void folder_name_changed();

protected:

private:
        vector<ConnectionN*> find_connection(Stop* start, Stop* endd);
        vector<ConnectionN*> find_2connection(Stop* start, Stop* endd);
        vector<ConnectionN*> find_3connection(Stop* start, Stop* endd);
        vector<ConnectionN*> find_4connection(Stop* start, Stop* endd);

        int get_stop_in_table_index(Stop* s);

        void create_connection_list();
        void create_2connection_list();
        void create_3connection_list();
        void create_4connection_list();
        void create_line(string filename);
        void create_stops(string filename);
        void create_walklines(string filename);
        void create_line_list();




};

bool sort_stoplist(Stop* a, Stop* b);


#endif // NETWORK_H
