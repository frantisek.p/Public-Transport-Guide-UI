#ifndef ROUTE_H
#define ROUTE_H

#include "Time_mh.h"
#include <vector>


class Route
{
    public:
        /** Default constructor */
        Route();
        /** Default destructor */
        virtual ~Route();
        vector <Time_mh> dep;
        int line_number=0;
        bool workday = 1;
        bool saturday = 0;
        bool sunday = 0;
        bool vacation = 0;
        string comment;
        int route_number=0;//equal the position in the route_vector in line

    protected:

    private:

        friend  bool operator== ( Route &a,  Route &b);

};


#endif // ROUTE_H

