#include "Time_mh.h"
 #include <cstdlib>

using namespace std;

Time_mh::Time_mh()
{
    //ctor
}
/*
Time_mh::Time_mh(Time_mh &t)
{
    hours = t.hours;
    minutes = t.minutes;
    all_minutes= t.all_minutes;
}*/

Time_mh::~Time_mh()
{
    //dtor
}

Time_mh::Time_mh (int _h, int _m)
{
    hours = _h;
    minutes = _m;
    all_minutes = 60*hours + minutes;
}

Time_mh::Time_mh (int _allm)
{
    hours = _allm/60;
    minutes = _allm%60;
    all_minutes = _allm;
}

Time_mh::Time_mh (std::string s)
{
    if(s.size()!=5 && s.size()!=4)
    {
        cerr << "Wrong input Time_mh" << endl;
        std::exit(1);
    }
    if(s.size()==4)
    {
        hours = std::stoi(s.substr(0,1));
        minutes = std::stoi(s.substr(2,2));
    }
    if(s.size()==5)
    {
        hours = std::stoi(s.substr(0,2));
        minutes = std::stoi(s.substr(3,2));
    }
    all_minutes = minutes + 60*hours;
}

QString Time_mh::FromTime_mhToQString()
{
    QString a;
    if(hours<10)
      a = a+ QString::number(0);
    a = a+ QString::number(hours) + QString::fromStdString(":");
    if(minutes<10)
        a = a+ QString::number(0);
    a = a+ QString::number(minutes);
    return a;
}

Time_mh  operator + (const Time_mh& a, const Time_mh& b)//sum of two time
{
    Time_mh res (a.all_minutes+b.all_minutes);

/*	Time_mh res;
	res.minutes = a.minutes + b.minutes;
	int over = 0;
	if (res.minutes>=60)
    {
        res.minutes %=60;
        over = 1;
    }
    res.hours = a.hours + b.hours + over;
    res.hours %= 24;*/
    return res;
}

Time_mh  operator - (const Time_mh& a, const Time_mh& b)//sum of two time
{
    Time_mh res ((int) std::abs(a.all_minutes - b.all_minutes));
/*
    Time_mh res;
    Time_mh _a= a;
    Time_mh _b = b;
    int over=0;
	if (a<b)
    {
    _b = a;
    _a = b;
    }
        if(_a.minutes >= _b.minutes)
            res.minutes = _a.minutes-_b.minutes;
            else
            {
             res.minutes = _a.minutes - _b.minutes + 60;
             over = 1;
            }
    res.hours = _a.hours - _b.hours + over;*/
    return res;
}

 bool operator>(Time_mh &a, Time_mh &b)
{
    if(a.all_minutes>b.all_minutes)
        return true;
    else
        return false;
}

 bool operator< ( Time_mh &a,  Time_mh &b) {return (b>a);}
 bool operator>= ( Time_mh &a,  Time_mh &b) {return !(a<b);}
 bool operator<= ( Time_mh &a,  Time_mh &b) {return !(a>b);}
 bool operator== ( Time_mh &a,  Time_mh &b) {return ((b<=a)&&(a<=b));}
 bool operator!= ( Time_mh &a,  Time_mh &b) {return !(b==a);}

ostream& operator<< (ostream& out, const Time_mh& num)//output of the number
{
    if(num.hours<10)
      out << 0;
	out << num.hours <<":";
	if(num.minutes<10)
        out << 0;
    out << num.minutes;
	return out;
}

 QStringList& operator<< (QStringList& out, const Time_mh& num)
 {
     QString a;
     if(num.hours<10)
       a = a+ QString::number(0);
     a = a+ QString::number(num.hours) + QString::fromStdString(":");
     if(num.minutes<10)
         a = a+ QString::number(0);
     a = a+ QString::number(num.minutes);
     out << a;
     return out;
 }




