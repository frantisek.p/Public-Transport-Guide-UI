#ifndef CONNECTION_H
#define CONNECTION_H
#include <Time_mh.h>
#include <Stop.h>
#include <Route.h>
#include <Line.h>
//#include "functions2.h"

//This class is used to show which objects Line connects two Stops not regardless any Time_mh
class Connection
{
    public:
        /** Default constructor */
        Connection();
        Connection(Stop* _start_stop,Stop* _end_stop, Line* line);
        /** Default destructor */
        virtual ~Connection();
        Stop* start_stop;
        Stop* end_stop;
        Line* line;
        vector<Stop*> connection_stops;
         bool goes_through(Stop* _stop);
         bool goes_from(Stop* _stop);/**< Give true, if the connecting lines goes though _stop before get on */
         bool goes_to(Stop* _stop);/**< Give true, if the connecting line goes through _stop after get off  */
         vector<Stop*> stops_before = {};
         vector<Stop*> stops_after = {};
         int stop_numbers =0;
        friend ostream& operator << (ostream & os, const Connection& c);


    protected:

    private:
};

bool operator == (const Connection& a, const Connection& b);
bool operator != (const Connection& a, const Connection& b);



#endif // CONNECTION_H
