#include "Journey.h"
#include "functions2.h"

Journey::Journey()
{
    //ctor
}

Journey::~Journey()
{
    //dtor
}

Journey::Journey(Connection& c , Time_mh departure_time)
{
    Time_mh t ("0:00");
    Time_mh tt ("99:00");
    Time_mh tt2 ("99:01");
    if(departure_time== t)
        std::exit(199);
    start_stop = c.start_stop;
    end_stop = c.end_stop;
    line = c.line;
    int index_start, index_end;
    index_start = f::gett_index(c.line->StopList,c.start_stop);
    index_end = f::gett_index(c.line->StopList,c.end_stop);
    if(index_start==-1 || index_end==-1 || index_start>index_end)
    {
        cout<< "connection constructor, connection not possible" << endl;
        std::exit(333);
    }
    for(int i=index_start;i<=index_end;i++)
            connection_stops.push_back(c.line->StopList[i]);
    for(int i=0;i<index_start;i++)
            stops_before.push_back(c.line->StopList[i]);
    for(unsigned int i=index_end+1;i<(c.line->StopList).size();i++)
            stops_after.push_back(c.line->StopList[i]);
    stop_numbers= index_end - index_start;
    //cout <<"before get route" <<endl;
    //HERE IS THE PROBLEM CONNECTION NOT POSSIBLE LEADS TO ZEROS IN TIME!!!
    route = line->get_route(departure_time,*start_stop);
    if(route==nullptr)
    {
        //cout << "is NULL" << endl;
        depart = tt;
        arrive = tt2;
        return;
    }
    depart = line->depart(departure_time,start_stop->Name);
    //arrive  = line->arrive(route->route_number,end_stop);
    arrive  = line->arrive(route,end_stop);
}


ostream& operator<<(ostream& os, Journey&  j)
{
    os << "Journey from "<< j.start_stop->Name << " at " << j.depart << " to " << j.end_stop->Name <<  " at " << j.arrive << " by line " << j.line->number;
    return os;
}

QStringList& operator<<(QStringList& os, Journey&  j)
{
    os <<  QString::fromStdString(j.start_stop->Name);
    os <<(j.depart);
    os << QString::fromStdString(j.end_stop->Name)<< j.arrive << QString::number(j.line->number);
    return os;
}
