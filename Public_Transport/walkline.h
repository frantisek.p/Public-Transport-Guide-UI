#ifndef WALKLINE_H
#define WALKLINE_H
#include "Line.h"
#include "Time_mh.h"
#include "Stop.h"
#include "Route.h"

using namespace std;

class WalkLine: public Line
{
public:
    WalkLine();
    WalkLine(int _number, vector <Stop*> _StopList, int _shield_number);
    Time_mh walk_time;
    //string type ="Pesi";
    /*Line();
    virtual ~Line();
    Line(int _number, vector <Stop*> _StopList, int _shield_number);
    int number;
    vector <Stop*> StopList;
    int shield_number = number/100;
    vector <Route*> routes;*/
    Time_mh depart(Time_mh& time, string& stop);
    Time_mh arrive(int route_num, Stop* s );
    Time_mh arrive(Route* route, Stop* s );
    /*int find_stop(string Name);
    bool connect (string a, string b);
    bool goes_through(Stop* _stop);*/
    Route* get_route(Time_mh& time, Stop& stop );


};

#endif // WALKLINE_H
