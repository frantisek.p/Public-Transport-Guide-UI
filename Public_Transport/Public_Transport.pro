QT += quick
QT += quickcontrols2
QT += widgets

CONFIG += c++11

# You can make your code fail to compile if it uses deprecated APIs.
# In order to do so, uncomment the following line.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

SOURCES += \
        ConnecionN.cpp \
        Connection.cpp \
        Journey.cpp \
        JourneyN.cpp \
        Line.cpp \
        Network.cpp \
        Route.cpp \
        Stop.cpp \
        Time_mh.cpp \
        functions2.cpp \
        journeylist.cpp \
        main.cpp \
        walkline.cpp

RESOURCES += qml.qrc \
    #resources.rc


RC_FILE = resources.rc

# Additional import path used to resolve QML modules in Qt Creator's code model
QML_IMPORT_PATH =

# Additional import path used to resolve QML modules just for Qt Quick Designer
QML_DESIGNER_IMPORT_PATH =

#QML_IMPORT_TRACE = 1

# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target

DISTFILES += \
    Line.qml \
    arrow.png \
    red_arrow.png \
    ikon.png

HEADERS += \
    Connection.h \
    ConnectionN.h \
    Journey.h \
    JourneyN.h \
    Line.h \
    Network.h \
    Route.h \
    Stop.h \
    Time_mh.h \
    functions2.h \
    journeylist.h \
    walkline.h
