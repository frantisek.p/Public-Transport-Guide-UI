import QtQuick 2.12
import QtQuick.Window 2.12
import QtQuick.Controls 2.12
import QtQuick.Layouts 1.12
import QtQuick.Dialogs 1.2








ApplicationWindow {
    id: wind2
    width: (780>Screen.desktopAvailableWidth*0.8)?Screen.desktopAvailableWidth*0.8:780
    minimumHeight: (780>Screen.desktopAvailableHeight*0.8)?Screen.desktopAvailableHeight*0.8:780
    minimumWidth: (780>Screen.desktopAvailableWidth*0.8)?Screen.desktopAvailableWidth*0.8:780
    height: (780>Screen.desktopAvailableHeight*0.8)?Screen.desktopAvailableHeight*0.8:780
    visible: true
    title: qsTr("Public Transport Searcher")
    property string oz: "Odjezdová zastávka:"
    property string pz: "Příjezdová zastávka:"
    property string co: "Čas:"
    property date currentDate: new Date()
    property bool initialised: false

    header: TabBar {
           id: bar
           width: parent.width

           TabButton {
               text: "Vyhledávání spojení"
               width: implicitWidth

               onClicked: {
                   console.log("Tab 1 was clicked");
               }
           }
           TabButton {
               text: "Vedení linek"
               width: implicitWidth
               onClicked: {
                   console.log("Tab 2 was clicked");
               }
           }
       }
    footer:


        Rectangle{
            //anchors.margins: 50
            id: autor
            height: textfp.height*2

            Rectangle{




            //color: blue
            //Layout.fillWidth: true
            anchors.leftMargin: 10
            anchors.rightMargin: 10
            anchors.bottomMargin: 5
            anchors.bottom: autor.bottom
            anchors.left: autor.left
            anchors.right: autor.right
            anchors.top: autor.top
            color: "lightgrey"
            radius: 5

            Text{
                id: textfp
                text: "© František Prinz 2021"
                horizontalAlignment: Text.right
                anchors.verticalCenter: parent.verticalCenter
                anchors.right: parent.right
                anchors.rightMargin: 20
                font.pointSize: 8
            }
            Text{
                text: "Public Transport Searcher - Version 0.2"
                horizontalAlignment: Text.left
                anchors.verticalCenter: parent.verticalCenter
                anchors.left: parent.left
                anchors.leftMargin: 20
                font.pointSize: 8
            }
            }
        }


    onAfterSynchronizing: {
        if (!initialised) {
            print("initialising...");
            _network.set_folder_name();
            //_network.load("timetables/loadfile.txt");
            //console.log( _network.folder_name+"/"+"loadfile.txt");
            _network.load(_network.folder_name);
            //windd.visible = false;
            tab11.a.visible = false;
            console.log("Height = " + Screen.desktopAvailableHeight);
            console.log("Width  = " + Screen.desktopAvailableWidth);
            initialised = true;
        }}

StackLayout {
    id: sv
    currentIndex: bar.currentIndex
    anchors.fill: parent


Tab1 {
    id:tab11

}
Tab2 {
    id: tab22

}

}

FileDialog
{
    id: fd
    //fileMode: FileDialog.SaveFile
    selectExisting: false
    nameFilters: ["Text files (.txt)"]
    defaultSuffix: ".txt"
    onAccepted:
    {
        console.log(fileUrl);
        _network.save_journeis_to_file(fileUrl, tab11.prijezova_zastavka, tab11.odjezdova_zastavka, tab11.hodin, tab11.minut );
        close();
     //   var model = _network.qqjourneyslist;
        //_network.qqjourneyslist
    }
    onRejected:
    {
        close();
    }


}



}











            //}///GridLayout
      //  }





