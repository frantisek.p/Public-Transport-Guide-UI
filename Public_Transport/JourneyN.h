#ifndef JOURNEYN_H
#define JOURNEYN_H
#include <vector>
#include <Journey.h>
#include <Connection.h>
#include <Time_mh.h>
#include <ConnectionN.h>
#include <Journey.h>
#include <QList>
//#include <functions2.h>
#include <walkline.h>

using namespace std;

class JourneyN
{
    public:
        /** Default constructor */
        JourneyN();
        JourneyN(ConnectionN & con, Time_mh depart);

        ~JourneyN();
        ConnectionN *c;

        vector <Journey*> journeis = {};
        Time_mh depart;
        Time_mh arrive;

    protected:

    private:
        void walkline_check(); //checks if starting journey is walkline and set right times
};

ostream& operator<<(ostream& os, JourneyN&  j);

QStringList& operator<<(QStringList& os, JourneyN&  j);

bool sort_arrival(JourneyN* a, JourneyN* b);

#endif // JOURNEYN_H
