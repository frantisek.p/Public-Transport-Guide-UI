#include "Network.h"
#include <functions2.h>
#include <typeinfo>


using namespace std;

Network::Network()
{
    //ctor
}

Network::~Network()
{
    //dtor
}

Network::Network(const Network& other)
{
    //copy ctor
}

Network& Network::operator=(const Network& rhs)
{
    if (this == &rhs) return *this; // handle self assignment
    //assignment operator
    return *this;
}

void Network::create_stops(string filename)
{
    cout << "create_stops starting " << filename << endl;
    fstream is;
    is.open(filename, ios::in);
    string line = "";
    string word;
    int row_number=0;
    int stop_index=0;
     while (getline(is, line)) {
        if (line.substr(0,1)=="#")
        {
            continue;
        }
        row_number++;
        //cout << row_number << endl;
        stringstream s(line);
        getline(s, word, ';');
        //cout << word << endl;
        Stop* _stop = new Stop;
        _stop->Name = word;
        _stop->Id = stop_index;
        stop_index++;
        getline(s,word,';');
        //cout << word << endl;
        _stop->change_time = Time_mh(0,stoi(word));
        getline(s,word,';');
        _stop->change_stop = stoi(word);
        if(  getline(s,word,';'))
        {
            cout << "network::create_stops error" << endl;
            std::exit(99);
        }
        stops.push_back(_stop);
        if(_stop->change_stop==1)
            change_stops.push_back(_stop);
        //cout << "stop name " << stops[row_number-1]->Name << endl;
     }
     cout << stops.size() << endl;
     for (int i=0;i<stops.size();i++)
       {  cout << stops[i]->Name << endl;
           cout << stops[i]->Id << endl;
     }
     sort(stops.begin(),stops.end(), sort_stoplist);
     cout << "after sort " << stops.size() << endl;
     for (int i=0;i<stops.size();i++)
        { cout << stops[i]->Name << endl;
        cout << stops[i]->Id << endl;
     }
     cout << "create_stops finishing " << filename << endl;




}

void Network::create_walklines(string filename)
{
    cout << "create_walklines starting " << filename << endl;
    fstream is;
    string word;
    is.open(filename, ios::in);

    string line = "";
   // int row_number= 0;
    while (getline(is, line)) {
        //cout << line << endl;
        if (line.substr(0,1)=="#")
        {
            continue;
        }
    WalkLine *_l = new WalkLine();
    stringstream s(line);
    getline(s, word, ';');
    cout << word << endl;
    _l->number = stoi(word);
    getline(s, word, ';');
    cout << word << endl;
    Stop* stop = new Stop(word);
    for(unsigned int i=0;i<stops.size();i++)
    {

        if(stops[i]->Name == word)
    {
        //cout<< stops[i]->Name << endl;
        stops[i]->line_numbers.push_back(_l->number);
        stop = stops[i];
        _l->StopList.push_back(stop);
        break;
    }}
    getline(s, word, ';');
    cout << word << endl;
    Stop* stop2 = new Stop(word);
    for(unsigned int i=0;i<stops.size();i++)
    {

        if(stops[i]->Name == word)
    {
        //cout<< stops[i]->Name << endl;
        stops[i]->line_numbers.push_back(_l->number);
        stop2 = stops[i];
        _l->StopList.push_back(stop2);
        break;
    }}
    getline(s, word, ';');
    cout << word << endl;
    int min = stoi(word);
  Time_mh t (0,min);
  _l->walk_time = t;
  lines.push_back(_l);
    }
  cout << "create walklines finished" << endl;



}

void Network::create_line_list()
{
    m_qlines.clear();
     m_routes_in_line.clear();
    for( unsigned int i=0; i<lines.size();i++)
    {
       if(typeid (*lines[i]) != typeid(WalkLine))

       m_qlines.push_back(QString::fromStdString(std::to_string(lines[i]->shield_number)));
    }
    m_qlines.removeDuplicates();
    m_qlines.sort();
    m_routes_in_line.reserve(m_qlines.size());
    for(int k=0;k<m_qlines.size();k++)
        m_routes_in_line.push_back({});
    for( unsigned int i=0; i<lines.size();i++)
    {
       if(typeid (*lines[i]) == typeid(WalkLine))
           continue;
       else
    {
           for( unsigned int j=0; j<m_qlines.size();j++)
        {
             if(QString::fromStdString(std::to_string(lines[i]->shield_number)) == m_qlines[j])
            {
                 //cout <<"if " << i << " " << j << endl;
                 m_routes_in_line[j].push_back(QString::fromStdString(std::to_string(lines[i]->number)));
                 break;
             }
           }

       }



    }
    for(int i=0;i<m_routes_in_line.size();i++)
        m_routes_in_line[i].sort();

}

void Network::create_line(std::string filename)
{
    cout << "create_line starting " << filename << endl;
    fstream is;
    is.open(filename, ios::in);
    Line* _line = new Line();
    string line = "";
    int row_number= 0;
    while (getline(is, line)) {
        //cout << line << endl;
        if (line.substr(0,1)=="#")
        {
            continue;
        }
        row_number++;
        Route* _route = new Route();

        stringstream s(line);
        string word = {};
       if (row_number==1)
        {
            //cout << "if " << row_number << endl;
            getline(s,word,';');
            _line->number = stoi(word);
            getline(s,word,';');
            _line->shield_number = stoi(word);
        }
        else if(row_number==2)
        {
            getline(s,word,';');
            getline(s,word,';');
            getline(s,word,';');
            getline(s,word,';');
            while(getline(s,word,';'))
            {
                Stop* stop = new Stop(word);
                for(unsigned int i=0;i<stops.size();i++)
                {

                    if(stops[i]->Name == word)
                {
                    //cout<< stops[i]->Name << endl;
                    stops[i]->line_numbers.push_back(_line->number);
                    stop = stops[i];
                    break;
                }
                    if(i==stops.size()-1)
                    {

                        cout << "Network::create_line " << filename << " stops error, stop not found, with stop " << word << endl;
                        std::exit(99);
                    }
                }
                _line->StopList.push_back(stop);

            }

        }
        else
        {
            getline(s,word,';');
             _route->workday = stoi(word);
             getline(s,word,';');
             _route->saturday = stoi(word);
             getline(s,word,';');
             _route->sunday = stoi(word);
             getline(s,word,';');
             _route->vacation = stoi(word);
             vector <string> columns;
        while( getline(s,word,';'))
        {
            columns.push_back(word);
        }
        for(unsigned int i=0;i<columns.size();i++)
        {
            //cout << columns[i] << endl;
           Time_mh t(columns[i]);
           _route->dep.push_back(t);
        }
        //_route->comment = columns[columns.size()-1];
        _route->line_number = _line->number;
        _line->routes.push_back(_route);
        _route->route_number=(_line->routes).size()-1;

        }


    }
    lines.push_back(_line);
     cout << "create_line finished " << filename << endl;
}

Stop* Network::get_stop(int Id)
{
    for(unsigned int i=0;i<(stops).size();i++)
        if(Id == stops[i]->Id)
        {
            return stops[i];
        }
        return NULL;
    std::cout << "Stop not found" << endl;
    std::exit(92);
}

Stop* Network::get_stop(string name)
{
    for(unsigned int i=0;i<(stops).size();i++)
        if(name == stops[i]->Name)
            return stops[i];
    std::cout << "Stop not found" << endl;
    std::exit(92);
}

 Line* Network::get_line (int number)
 {
        for(unsigned int i=0;i<(lines).size();i++)
        if(number == lines[i]->number)
            return lines[i];
    std::cout << "Stop not found" << endl;
    std::exit(92);
 }

 vector<ConnectionN*> Network::find_connection(Stop* start, Stop* endd)
 {
     vector <ConnectionN*> con_lines;
     for(unsigned int i=0;i<lines.size();i++)
     {

         Line *l = new Line();
         *l= *lines[i];
         int st, ed;
         ed=0;
         st=100;
         for(unsigned int j=0;j<l->StopList.size();j++)
         {

            if(l->StopList[j]==start)
            {
                st=j;
            }
            if(l->StopList[j]==endd)
            {
                ed = j;
                break;
            }
         }
         if(st<ed)
         {
             //cout << "if connection creating" << endl;
              Connection *c = new Connection(start, endd, lines[i]);
              ConnectionN *cc = new ConnectionN();
              cc->push_connection(c);
              con_lines.push_back(cc);
         }


     }
    return con_lines;
 }

 vector<ConnectionN*> Network::find_2connection (Stop* start, Stop* endd)
 {
     vector<Stop*> stops_temp = stops;
     vector<ConnectionN*> conN;
     for(int i=stops_temp.size()-1;i>=0;i--)
        if(stops_temp[i]->change_stop==0 || stops_temp[i]==start || stops_temp[i]==endd)
     {
       // iterator it=i;
            stops_temp.erase(stops_temp.begin()+i);
            }
            //cout << stops_temp.size() << endl;
    //for(int i=0;i<stops_temp.size();)
     for(unsigned int i=0;i<stops_temp.size();i++)
 {
        vector <ConnectionN*> c1 = get_connection_from_table(start, stops_temp[i]);//find_connection(start,stops_temp[i]);
        vector <ConnectionN*> c2 = get_connection_from_table(stops_temp[i],endd);//find_connection(stops_temp[i], endd);
        for(unsigned int j=0;j<c1.size();j++)
            for(unsigned int k=0;k<c2.size();k++)
        {
            if((c1[j]->con[0]->line) != (c2[k]->con[0]->line) && !(c1[j]->con[0]->goes_through(endd))&& !(c2[k]->con[0]->goes_through(start)) &&
               !(c1[j]->con[0]->goes_to(endd)) && !(c2[k]->con[0]->goes_from(start)))
            {
                ConnectionN* cN = new ConnectionN();
                cN->push_connection(c1[j]->con[0]);
                cN->push_connection(c2[k]->con[0]);
                conN.push_back(cN);
            }

        }}
     remove_duplicate_connections(conN);
     return conN;
 }

void Network::create_connection_list()
{
    cout << "Creating connection list started..." << endl;
    int ssize = (this->stops).size();
    vector<vector<vector<ConnectionN*>>> c (ssize);
    for(unsigned int i=0;i<c.size();i++)
        c[i]=vector<vector<ConnectionN*>> (ssize);

    for(unsigned int i=0;i<c.size();i++)
        for(unsigned int j=0;j<c.size();j++)
            if(i!=j)
            {
                c[i][j] = this->find_connection(this->stops[i],this->stops[j]);
               /* if(c[i][j].size()!=0)
                cout << i <<" " <<j<< " "<<   c[i][j][0]->line->number << endl;*/
            }
    connection_list = c;
    cout << "Creating connection list finished" << endl;
}

void Network::create_2connection_list()
{
    cout << "Creating 2 connection list started..." << endl;
    int ssize = (this->stops).size();
    vector<vector<vector<ConnectionN*>>> c (ssize);
    //cout << c.size() << endl;
    for(unsigned int i=0;i<c.size();i++)
        c[i]=vector<vector<ConnectionN*>> (ssize);
       // cout << c[0].size() <<endl;
        //c[12][23];

    for(unsigned int i=0;i<c.size();i++)
        for(unsigned int j=0;j<c.size();j++)
            if(i!=j)
            {
                c[i][j] = this->find_2connection(this->stops[i],this->stops[j]);
                //if(c[i][j].size()!=0)
                //cout << i <<" " <<j<< " "<<   c[i][j][0]->line->number << endl;
            }
    connection2_list = c;
    cout << "Creating 2 connection list finished" << endl;
}

 vector<ConnectionN*> Network::find_3connection(Stop* start, Stop* endd)
 {
     vector<ConnectionN*> c1;
     c1.reserve(1000);
     vector<ConnectionN*> c2;
     c2.reserve(1000);
     vector<ConnectionN*> output1;
     output1.reserve(1000);
    for(unsigned int i=0;i<change_stops.size();i++)
    {
        if(change_stops[i]!=start && change_stops[i]!=endd)
        {
       c1 = get_connection_from_table(start,change_stops[i]);
       c2 = get_2connection_from_table(change_stops[i],endd);
        }
    for(unsigned int k=0;k<c1.size();k++)
        for(unsigned int l=0;l<c2.size();l++)
        {
            ConnectionN* conN = new ConnectionN();
            conN->push_connection(c1[k]->con[0]);
            conN->push_connection(c2[l]->con[0]);
            conN->push_connection(c2[l]->con[1]);
            output1.push_back(conN);
        }

    }
    vector<ConnectionN*> d2;
    d2.reserve(1000);
     vector<ConnectionN*> d1;
     d1.reserve(1000);
     vector<ConnectionN*> output2;
     output2.reserve(1000);
    for(unsigned int i=0;i<change_stops.size();i++)
    {
        if(change_stops[i]!=start && change_stops[i]!=endd)
        {
       d1 = get_2connection_from_table(start,change_stops[i]);
       d2 = get_connection_from_table(change_stops[i],endd);
        }
    for(unsigned int k=0;k<d2.size();k++)
        for(unsigned int l=0;l<d1.size();l++)
        {
            ConnectionN* conN = new ConnectionN();
            conN->push_connection(d1[l]->con[0]);
            conN->push_connection(d1[l]->con[1]);
            conN->push_connection(d2[k]->con[0]);
            output2.push_back(conN);
        }

    }

    vector <ConnectionN*> result;
   for(unsigned int i=0;i<output1.size();i++)
        for(unsigned int j=0;j<output2.size();j++)
            if(*output1[i]==*output2[j]&&!output1[i]->con[0]->goes_to(endd)&&!output1[i]->con[2]->goes_through(start)&&!output1[i]->duplicate_changing_stops())
        {
            result.push_back(output1[i]);
            break;
        }
 /*  if(start->Name == "Valticka"&& endd->Name == "PKZ")
{
       for(int i=0;i<result.size();i++)
           cout <<*result[i]<<endl;
       cout << result.size() << endl;
}*/

    return result;
 }

 vector<ConnectionN*> Network::find_4connection(Stop* start, Stop* endd)
 {
     vector<ConnectionN*> c1;
     c1.reserve(1000);
     vector<ConnectionN*> c2;
     c2.reserve(1000);
     vector<ConnectionN*> output1;
     output1.reserve(1000);
    for(unsigned int i=0;i<change_stops.size();i++)
    {
        if(change_stops[i]!=start && change_stops[i]!=endd)
        {
       c1 = get_connection_from_table(start,change_stops[i]);
       c2 = get_3connection_from_table(change_stops[i],endd);
        }
    for(unsigned int k=0;k<c1.size();k++)
        for(unsigned int l=0;l<c2.size();l++)
        {
            ConnectionN* conN = new ConnectionN();
            conN->push_connection(c1[k]->con[0]);
            conN->push_connection(c2[l]->con[0]);
            conN->push_connection(c2[l]->con[1]);
            conN->push_connection(c2[l]->con[2]);
            output1.push_back(conN);
        }

    }
    vector<ConnectionN*> d2;
    d2.reserve(1000);
     vector<ConnectionN*> d1;
     d1.reserve(1000);
     vector<ConnectionN*> output2;
     output2.reserve(1000);
    for(unsigned int i=0;i<change_stops.size();i++)
    {
        if(change_stops[i]!=start && change_stops[i]!=endd)
        {
       d1 = get_3connection_from_table(start,change_stops[i]);
       d2 = get_connection_from_table(change_stops[i],endd);
        }
    for(unsigned int k=0;k<d2.size();k++)
        for(unsigned int l=0;l<d1.size();l++)
        {
            ConnectionN* conN = new ConnectionN();
            conN->push_connection(d1[l]->con[0]);
            conN->push_connection(d1[l]->con[1]);
            conN->push_connection(d1[l]->con[2]);
            conN->push_connection(d2[k]->con[0]);
            output2.push_back(conN);
        }

    }
    //TODO Reduce 4connections by removing twice driven through stops
    vector <ConnectionN*> result;
   for(unsigned int i=0;i<output1.size();i++)
        for(unsigned int j=0;j<output2.size();j++)
            if(*output1[i]==*output2[j]&&!output1[i]->con[0]->goes_to(endd)&&!output1[i]->con[3]->goes_through(start)&&!output1[i]->duplicate_changing_stops())
        {
            result.push_back(output1[i]);
            break;
        }
  /* if(start->Name == "Valticka tocna"&& endd->Name == "PKZ")
  {
         for(int i=0;i<result.size();i++)
            {
             cout <<*result[i]<<endl;
             //for(int j=0;j<result[i]->connection_stops.size();j++)
                // cout <<result[i]->connection_stops[j]->Name << endl;
            }
         cout << result.size() << endl;
  }*/
   return result;
 }

 int Network::get_stop_in_table_index(Stop *s)
 {
     int i=0;
    for(i;i<stops.size();i++)
        if(s==stops[i])
            return i;
    cout << "Table index not found "<< endl;
    return -1;
 }


 void Network::create_3connection_list()
 {
     cout << "Creating 3 connection list started..." << endl;
     int ssize = (this->stops).size();
    vector<vector<vector<ConnectionN*>>> c (ssize);
    //cout << c.size() << endl;
    for(unsigned int i=0;i<c.size();i++)
        c[i]=vector<vector<ConnectionN*>> (ssize);
       // cout << c[0].size() <<endl;
        //c[12][23];


    for(unsigned int i=0;i<c.size();i++)
        for(unsigned int j=0;j<c.size();j++)
            if(i!=j)
            {
                //cout << stops[i]->Name << " " << stops[j]->Name << endl;
                c[i][j] = this->find_3connection(this->stops[i],this->stops[j]);
                //if(c[i][j].size()!=0)
                //cout << i <<" " <<j<< " "<<   c[i][j][0]->line->number << endl;
            }
    connection3_list = c;
    cout << "Creating 3 connection list finished" << endl;
 }

  void Network::create_4connection_list()
 {
     cout << "Creating 4 connection list started..." << endl;
     int ssize = (this->stops).size();
    vector<vector<vector<ConnectionN*>>> c (ssize);
    //cout << c.size() << endl;
    for(unsigned int i=0;i<c.size();i++)
        c[i]=vector<vector<ConnectionN*>> (ssize);
       // cout << c[0].size() <<endl;
        //c[12][23];


    for(unsigned int i=0;i<c.size();i++)
        for(unsigned int j=0;j<c.size();j++)
            if(i!=j)
            {
                //cout << stops[i]->Name << " " << stops[j]->Name << endl;
                c[i][j] = this->find_4connection(this->stops[i],this->stops[j]);
                //if(c[i][j].size()!=0)
                //cout << i <<" " <<j<< " "<<   c[i][j][0]->line->number << endl;
            }
    connection4_list = c;
    cout << "Creating 4 connection list finished..." << endl;
 }

 vector<ConnectionN*>& Network::get_connection_from_table(Stop* start, Stop* endd)
 {
     int i,j;
     i = get_stop_in_table_index(start);
     j = get_stop_in_table_index(endd);
     return connection_list[i][j];
 }

 vector<ConnectionN*>& Network::get_2connection_from_table(Stop* start, Stop* endd)
 {
     int i,j;
     i = get_stop_in_table_index(start);
     j = get_stop_in_table_index(endd);
     return connection2_list[i][j];
 }

 vector<ConnectionN*>& Network::get_3connection_from_table(Stop* start, Stop* endd)
 {
     int i,j;
     i = get_stop_in_table_index(start);
     j = get_stop_in_table_index(endd);
     return connection3_list[i][j];
 }

 vector<ConnectionN*>& Network::get_4connection_from_table(Stop* start, Stop* endd)
 {
     int i,j;
     i = get_stop_in_table_index(start);
     j = get_stop_in_table_index(endd);
     return connection4_list[i][j];
 }

 vector<JourneyN*> Network::find_journeys (Stop* start, Stop* endd, Time_mh depart)
 {
    cout <<"Searching for journeys from " << start->Name << " to " << endd->Name << endl;
    vector<JourneyN*> result;
    vector<ConnectionN*> a1, a2, a3, a4;
    a1= get_connection_from_table(start, endd);
    a2= get_2connection_from_table(start, endd);
    a3= get_3connection_from_table(start, endd);
    a4= get_4connection_from_table(start, endd);
    for(unsigned int i=0;i<a1.size();i++)
    {
        JourneyN * j = new JourneyN(*a1[i],depart);
        result.push_back(j);
    }
    for(unsigned int i=0;i<a2.size();i++)
    {
        JourneyN * j = new JourneyN(*a2[i],depart);
        result.push_back(j);
    }
    for(unsigned int i=0;i<a3.size();i++)
    {
        JourneyN * j = new JourneyN(*a3[i],depart);
        result.push_back(j);
    }
    for(unsigned int i=0;i<a4.size();i++)
    {
        JourneyN * j = new JourneyN(*a4[i],depart);
        result.push_back(j);
    }

    return result;
 }


QStringList Network::get_qstops()
 {
     QStringList list;
     for(unsigned int i=0;i<stops.size();i++)
         list << QString::fromStdString(stops[i]->Name);
     return list;

 }

 QStringList Network::get_qqst()
 {
     cout << "get_qqst called" << endl;
     return qst;
 }

 QList<int> Network::get_qjourneys_no()
 {
     QList<int> output;
     for(unsigned int i=0;i<journeys_no.size();i++)
         output.push_back(journeys_no[i]);
     return output;
 }

void Network::vyhledat(int ifrom, int ito, int hod, int min)
{
    qst.clear();

    from = stops[ifrom];
    cout << "VYHLEDAT" << endl;
    cout << from->Name << endl;
    to = stops[ito];
    cout << to->Name << endl;
    time = Time_mh(hod, min);
    journeys = this->find_journeys(from, to, time);
    create_journeys(10);
    create_qjourneyslist();
    //f::print_journey(journeys, 5, std::cout);
    emit qqjourneyslist_changed();
    emit qqst_changed();
}


void Network::save_journeis_to_file( QUrl source, const QString from, const QString to, int hod, int min)
{
    if (source.toLocalFile().isEmpty())
    {cout << "if1 is empty" << endl;
        return ;}

    QFile file(source.toLocalFile());
    if (!file.open(QFile::WriteOnly | QFile::Truncate))
    {cout << "if1 is empty" << endl;
        return ;}
    Time_mh a(hod,min);
    QTextStream out(&file);
    out << QString::fromStdString("Výsledky hledání spojení z ") << from << " do " << to << QString::fromStdString(" v čase ") <<a.FromTime_mhToQString()<<  Qt::endl <<Qt::endl;
    for (int i =0;i<this->qjourneyslist.length();i++)
    {
        for (int j=0; j<qjourneyslist[i]->m_vector.size();j++)
           {out << qjourneyslist[i]->m_vector[j].type << " ";
            out << qjourneyslist[i]->m_vector[j].lineno << Qt::endl;
            out << qSetFieldWidth(30) << Qt::left << qjourneyslist[i]->m_vector[j].startstop;
            out << qSetFieldWidth(15) <<qjourneyslist[i]->m_vector[j].starttime;
            out << qSetFieldWidth(30) << Qt::left << qjourneyslist[i]->m_vector[j].endstop;
            out << qSetFieldWidth(5) <<qjourneyslist[i]->m_vector[j].endtime;
            out << qSetFieldWidth(1)<< Qt::endl;

            }
        out << Qt::endl;
    }
    //out << qjourneyslist[i]->LineNoRole << Qt::endl;
    file.close();
    return;
}

Q_INVOKABLE void Network::load()
{
    create_stops("timetables/stops.csv");
    create_line("timetables/56001.csv");
    create_line("timetables/56002.csv");
    create_line("timetables/56003.csv");
    create_line("timetables/56004.csv");
    create_line("timetables/56005.csv");
    create_line("timetables/56006.csv");
    create_line("timetables/56101.csv");
    create_line("timetables/56103.csv");
   create_line("timetables/56105.csv");
    create_line("timetables/56102.csv");
    create_line("timetables/56104.csv");
    create_line("timetables/56106.csv");
    create_line("timetables/56201.csv");
    create_line("timetables/56202.csv");
    create_line("timetables/56301.csv");
    create_line("timetables/56302.csv");
    create_line("timetables/56601.csv");
    create_line("timetables/56602.csv");
    create_line("timetables/56801.csv");
    create_line("timetables/56802.csv");
    create_line("timetables/56401.csv");
    create_line("timetables/56402.csv");
    create_line("timetables/56501.csv");
    create_line("timetables/56502.csv");
    create_line("timetables/56701.csv");
    create_line("timetables/56703.csv");
    create_line("timetables/56702.csv");
    create_line("timetables/56704.csv");
    create_connection_list();
    create_2connection_list();
    create_3connection_list();
    //emit loader_state_changed();
    create_4connection_list();
    emit qstops_changed();
    create_line_list();
    emit qlines_changed();


}

void Network::loadfromstring(string filename)
{
    cout << "Loading files started..." << endl;
    fstream is;
    is.open(filename, ios::in);
    string line = "";
    int row_number= 0;
    while (getline(is, line)) {
        if (line.substr(0,1)=="#")
        {
            continue;
        }
        if(row_number ==0)
            create_stops("timetables/"+line);
        else
            create_line("timetables/"+ line);
        row_number++;
    }
    create_walklines("");
    create_connection_list();
    create_2connection_list();
    create_3connection_list();
    //set_loader_state(0.9);
    //emit loader_state_changed();
    create_4connection_list();
    cout << "Loading files end" << endl;

}

void Network::load(QString filename2)
{
    std::string filename = filename2.toStdString()+"/loadfile.txt";
    std::string folder = filename2.toStdString()+"/";
    cout << "Loading files started..." << endl;
    fstream is;
    is.open(filename, ios::in);
    string line = "";
    int row_number= 0;
    while (getline(is, line)) {
        if (line.substr(0,1)=="#")
        {
            continue;
        }
        if(row_number ==0)
            create_stops(folder+line);
        else
            create_line(folder+ line);
        row_number++;
    }
    create_walklines(folder + "walklines.csv");
    create_connection_list();
    create_2connection_list();
    create_3connection_list();
    create_4connection_list();
    cout << "Loading files end" << endl;
    emit qstops_changed();
    create_line_list();
    emit qlines_changed();
    emit routes_in_line_changed();
}

void Network::create_qlinestops(int line_number)
{
    Line* l = get_line(line_number);
    m_qlinestops.clear();
    for (int i=0;i<l->StopList.size();i++)
        m_qlinestops.push_back(QString::fromStdString(l->StopList[i]->Name));
    emit qlinestops_changed();
}

void Network::set_folder_name()
{
    cout << "Starting saving folder name..." << endl;
    fstream is;
    is.open("folder_name.txt", ios::in);
    string line = "";
    //int row_number= 0;
    while (getline(is, line)) {
        cout << line << endl;
        if (line.substr(0,1)=="#")
        {
            continue;
        }
    this->folder_name = QString::fromStdString(line);
    }
    //cout << this->folder_name.toStdString() << endl;
    emit folder_name_changed();
}

void Network::create_journeys(int i_max)
{


     cout <<"Printing sorted journeys" <<endl;
     journeys_output.clear();
     Time_mh timer ("00:00");

    sort(journeys.begin(),journeys.end(),sort_arrival);
     for(int i=0;i<i_max;i++)
     {

    sort(journeys.begin(),journeys.end(),sort_arrival);
     sort(journeys.begin(),journeys.end(), sort_arrival);
     if(journeys.size()>=1)
     {
     for(unsigned int n=0;n<journeys.size();n++)
            if(journeys[n]->journeis[0]->depart <= timer)
     {
         Time_mh s = Time_mh("0:01");
        Time_mh t = timer + s;
        journeys[n] = new JourneyN(*(journeys[n]->c),t);
     }
        sort(journeys.begin(),journeys.end(),sort_arrival);
     for(unsigned int m=1;m<journeys.size()-1;m++)
     {
        if((journeys[0]->arrive==journeys[0+m]->arrive))
        {
            Time_mh t = journeys[m]->journeis[0]->depart+ Time_mh("0:01");
            journeys[m] = new JourneyN(*(journeys[m]->c),t);
            t = Time_mh("60:00");
        }
        else
            break;
     }
     }
     sort(journeys.begin(),journeys.end(),sort_arrival);
     std::string aaa = "25:00";
     Time_mh ta (aaa);
     timer =  journeys[0]->journeis[0]->depart;
     for( int s= 0;s<journeys.size();s++)
        {
         if(journeys[s]->journeis[0]->depart == timer)
            {
             Time_mh ztimerr("00:01");
             Time_mh ztimerr2 = journeys[s]->journeis[0]->depart;
             Time_mh tf ("24:00");
             if(journeys[s]->journeis[0]->depart < tf )
            {
             Time_mh sum = ztimerr + ztimerr2;
             JourneyN* jj = new JourneyN(*(journeys[s]->c),sum);
             if(jj->arrive == journeys[s]->journeis[journeys[s]->journeis.size()-1]->arrive/* && jj->arrive < tf*/ )
                {
                 journeys[s] = jj;
             }

         }
         }
     else
             break;
     }
     sort(journeys.begin(),journeys.end(),sort_arrival);
     JourneyN * a = new JourneyN(*journeys[0]);
     if(a->journeis[0]->depart < (ta))//prevent to appending fictive connections when no connection exists
     journeys_output.push_back(a);
     Time_mh s = Time_mh("0:01");
     Time_mh t = journeys[0]->journeis[0]->depart+ s;
     journeys[0] = new JourneyN(*(journeys[0]->c),t);
     }
}

void Network::create_qjourneyslist()
{
    qjourneyslist.clear();
    for(unsigned int j=0;j<journeys_output.size();j++)
{
    JourneyList *a = new JourneyList;
    for(unsigned int i=0;i<journeys_output[j]->journeis.size();i++)
    {
        JourneyListItem* jli = new JourneyListItem();
        jli->starttime = (journeys_output[j]->journeis[i]->depart).FromTime_mhToQString();
        jli->endtime = (journeys_output[j]->journeis[i]->arrive).FromTime_mhToQString();
        jli->startstop = QString::fromStdString(journeys_output[j]->journeis[i]->start_stop->Name);
        jli->endstop = QString::fromStdString(journeys_output[j]->journeis[i]->end_stop->Name);
        jli->lineno = journeys_output[j]->journeis[i]->line->number;
        jli->type = QString::fromStdString(journeys_output[j]->journeis[i]->line->type);
        a->m_vector.push_back(*jli);
}
    qjourneyslist.push_back(a);
    }
}

bool sort_stoplist(Stop* a, Stop* b)
{
    return a->Name < b->Name;

}
