#include <ConnectionN.h>

void remove_duplicate_connections(vector<ConnectionN*> &con)
{
    bool change = true;
    while(change)
    {
    change = false;
    for(unsigned int i=0;i<con.size();i++)
    {


        for(unsigned int j=i;j<con.size();j++)
        {
            //cout << i << " " << j << endl;
            if(same_lines(*con[i],*con[j])&&i!=j)
            {
                //cout << "if for same lines" << endl;
                if((con[i]->stop_numbers) > (con[j]->stop_numbers))
                    con.erase(con.begin()+i);
                else
                    con.erase(con.begin()+j);
                change = true;
                break;
                break;
            }
            }}
    }
};


bool same_lines(ConnectionN& a, ConnectionN& b)
{
    if(a.con.size()!=b.con.size())
        return false;
    for(unsigned int i=0;i<a.con.size();i++)
        if((a.con[i])->line != (b.con[i])->line)
        return false;
    return true;
};

bool operator == (ConnectionN& a, ConnectionN& b)
{
    if(a.con.size() !=b.con.size())
        return false;
    for(unsigned int i=0;i<a.con.size();i++)
        if(*a.con[i] != *b.con[i])
        return false;
    return true;
}

bool ConnectionN::duplicate_changing_stops()
{
    for(int i=0;i<connection_stops.size();i++)
{
        int j;
        if(i !=connection_stops.size()-1)
            j = i+1;
        else
            continue;
        for(j;j<connection_stops.size();j++)
            if(connection_stops[i]==connection_stops[j]&&connection_stops[i]->change_stop==1&& j-i!=1)
                {
                return true;

                }
    }
    return false;
}
