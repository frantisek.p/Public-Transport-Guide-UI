#ifndef JOURNEYLIST_H
#define JOURNEYLIST_H

#include <QAbstractListModel>


struct JourneyListItem
{

    QString startstop;
    QString endstop;
    QString starttime;
    QString endtime;
    qint32 lineno;
    QString type;
    //int changes;
};


class JourneyList : public QAbstractListModel
{
    Q_OBJECT
    Q_ENUMS(MyRoles)

public:
    enum MyRoles
    {
        StartStopRole = Qt::UserRole,
                EndStopRole,
                StartTimeRole,
                EndTimeRole,
                LineNoRole,
                TypeRole
               // ChangesRole
    };

    explicit JourneyList(QObject *parent = nullptr);

    using QAbstractListModel::QAbstractListModel;

    QHash<int,QByteArray> roleNames() const override {
          return { { StartStopRole, "startstop" },
              { EndStopRole, "endstop" },
              { StartTimeRole, "starttime" },
              { EndTimeRole, "endtime" },
              { LineNoRole, "lineno" },
              { TypeRole, "type" },
              //{ ChangesRole, "changes" },
          };
      }
    // Basic functionality:
    int rowCount(const QModelIndex &parent = QModelIndex()) const override;

    QVariant data(const QModelIndex &index, int role = Qt::DisplayRole) const override;

    // Editable:
    bool setData(const QModelIndex &index, const QVariant &value,
                 int role = Qt::EditRole) override;

    QVector<JourneyListItem> m_vector {    };

private:
};

#endif // JOURNEYLIST_H
