#include "JourneyN.h"
#include "functions2.h"


JourneyN::JourneyN()
{
    //ctor
}

JourneyN::~JourneyN()
{
    //dtor
}

void JourneyN::walkline_check()
{
    if(journeis.size()<=1)
        return;
    if(typeid (*journeis[0]->line) == typeid(WalkLine))
    {
        Time_mh duration = journeis[0]->arrive - journeis[0]->depart;
        //cout << "typeid called" << endl;
        //Route r = *journeis[0]->line->get_route(journeis[1]->depart,*journeis[1]->start_stop);
        journeis[0]->arrive = journeis[1]->depart;
        journeis[0]->depart = journeis[0]->arrive - duration;
    }


}


JourneyN::JourneyN(ConnectionN& con, Time_mh depart)
{
    c = &con;
    for(unsigned int i=0;i<con.con.size();i++)
    {
        //cout << "for " << i << endl;
        if(i==0)
        {
        Journey* j = new Journey(*con.con[i],depart);
        journeis.push_back(j);
        }
        else
        {
        //Journey* j = new Journey(*con.con[i],journeis[i-1]->arrive);
        Journey* j = new Journey(*con.con[i],journeis[i-1]->arrive+journeis[i-1]->end_stop->change_time);
        journeis.push_back(j);
        }
    }
    int ssize = journeis.size();
    if(ssize==0)
        true;
    else if(ssize==1)
    {
        depart = journeis[0]->depart;
        arrive = journeis[0]->arrive;
    }
    else
    {
        depart = journeis[0]->depart;
        arrive = journeis[journeis.size()-1]->arrive;

    }
    walkline_check();


}

ostream& operator<<(ostream& os, JourneyN&  j)
{
    Time_mh s ("80:00");
    if(j.journeis[j.journeis.size()-1]->arrive > s )
    {
        os << "No travel found!"<< endl;
        return os;
    }
    for(unsigned int i=0;i<j.journeis.size();i++)
    os << *j.journeis[i] << endl;
    return os;
}

QStringList& operator<<(QStringList& os, JourneyN&  j)
{
    Time_mh s ("80:00");
    if(j.journeis[j.journeis.size()-1]->arrive > s )
    {
        os << QString::fromStdString("No travel found!")/*<< QString::endl*/;
        os << " ";
        return os;
    }
    for(unsigned int i=0;i<j.journeis.size();i++)
    os << *j.journeis[i] /*<< endl*/;
    os << " ";
    return os;
}

bool sort_arrival(JourneyN* a, JourneyN* b)
{
    int a_size = a->journeis.size();
    int b_size = b->journeis.size();
    if(a->journeis[a_size-1]->arrive < b->journeis[b_size-1]->arrive)
        return true;
    else
    {
    if(a->journeis[a_size-1]->arrive > b->journeis[b_size-1]->arrive)
        return false;
    }
    {
        if(a->journeis[0]->depart > b->journeis[0]->depart)
            return true;
        else if (a->journeis[0]->depart < b->journeis[0]->depart)
            return false;
        else
            return (a_size < b_size);
    }
}


