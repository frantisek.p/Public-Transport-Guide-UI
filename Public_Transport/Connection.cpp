#include "Connection.h"
#include "functions2.h"

using namespace std;

Connection::Connection()
{
    //ctor
}

Connection::~Connection()
{
    //dtor
}


Connection::Connection(Stop* _start_stop,Stop* _end_stop, Line* _line)
{
    start_stop = _start_stop;
    end_stop = _end_stop;
    line = _line;
    int index_start, index_end;
    index_start = f::gett_index(_line->StopList,_start_stop);
    index_end = f::gett_index(_line->StopList,_end_stop);
    if(index_start==-1 || index_end==-1 || index_start>index_end)
    {
        cout<< "connection constructor, connection not possible" << endl;
        std::exit(333);
    }
    for(int i=index_start;i<=index_end;i++)
            connection_stops.push_back(_line->StopList[i]);
    for(int i=0;i<index_start;i++)
            stops_before.push_back(_line->StopList[i]);
    for(unsigned int i=index_end+1;i<(_line->StopList).size();i++)
            stops_after.push_back(_line->StopList[i]);
    stop_numbers=index_end - index_start;
}


ostream& operator << (ostream & os, const Connection & c)
{
    os << "Connection from "<< c.start_stop->Name << " to " <<c.end_stop->Name << " by line " << c.line->number;
    return os;
}

bool operator == (const Connection& a, const Connection& b)
{
    if(a.start_stop != b.start_stop)
        return false;
    if(a.end_stop != b.end_stop)
        return false;
    if(a.line != b.line)
        return false;
    return true;
}

bool operator != (const Connection& a, const Connection& b)
{
    return !(a==b);
}


 bool Connection::goes_through(Stop* _stop)
 {
     if(f::gett_index(connection_stops,_stop)==-1)
        return false;
     return true;
 }

bool Connection::goes_from(Stop* _stop)
{
    if(f::gett_index(stops_before,_stop)==-1)
        return false;
     return true;
}

bool Connection::goes_to(Stop* _stop)
{
    if(f::gett_index(stops_after,_stop)==-1)
        return false;
     return true;
};/**< Give true, if the connecting line goes through _stop after get off  */
