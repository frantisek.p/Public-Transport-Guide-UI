#ifndef JOURNEY_H
#define JOURNEY_H

#include <Connection.h>

class Journey : public Connection
{
    public:
        /** Default constructor */
        Journey();
        /** Default destructor */
        virtual ~Journey();
        Time_mh depart;
        Time_mh arrive;
        Route* route;
        Journey(Connection& c , Time_mh departure_time);

    protected:

    private:
};

ostream& operator<<(ostream& os, Journey&  j);

QStringList& operator<<(QStringList& os, Journey&  j);

#endif // JOURNEY_H
