#include "journeylist.h"

JourneyList::JourneyList(QObject *parent)
    : QAbstractListModel(parent)
{
}

int JourneyList::rowCount(const QModelIndex &parent) const
{
    // For list models only the root node (an invalid parent) should return the list's size. For all
    // other (valid) parents, rowCount() should return 0 so that it does not become a tree model.
    if (parent.isValid())
        return 0;

    return m_vector.size();
}

QVariant JourneyList::data(const QModelIndex &index, int role) const
{
    if (!hasIndex(index.row(), index.column(), index.parent()))
                return {};

            const JourneyListItem &item = m_vector.at(index.row());
            if (role == StartStopRole) return item.startstop;
            if (role == EndStopRole) return item.endstop;
            if (role == StartTimeRole) return item.starttime;
            if (role == EndTimeRole) return item.endtime;
            if (role == LineNoRole) return item.lineno;
            if (role == TypeRole) return item.type;

            return {};
}

bool JourneyList::setData(const QModelIndex &index, const QVariant &value, int role)
{
        if (!hasIndex(index.row(), index.column(), index.parent()) || !value.isValid())
            return false;

        JourneyListItem &item = m_vector[index.row()];
        if (role == StartStopRole) item.startstop = value.toString();
        else if (role == EndStopRole) item.endstop = value.toString();
        else if (role == StartTimeRole) item.starttime = value.toString();
        else if (role == EndTimeRole) item.endtime = value.toString();
        else if (role == LineNoRole) item.lineno = value.toInt();
        else if (role == TypeRole) item.type = value.toString();
        else return false;


        emit dataChanged(index, index, QVector<int>() << role);
        return true;

}


