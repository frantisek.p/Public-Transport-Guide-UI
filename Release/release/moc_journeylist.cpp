/****************************************************************************
** Meta object code from reading C++ file 'journeylist.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.15.2)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include <memory>
#include "../../Public_Transport/journeylist.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'journeylist.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.15.2. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
QT_WARNING_PUSH
QT_WARNING_DISABLE_DEPRECATED
struct qt_meta_stringdata_JourneyList_t {
    QByteArrayData data[8];
    char stringdata0[92];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_JourneyList_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_JourneyList_t qt_meta_stringdata_JourneyList = {
    {
QT_MOC_LITERAL(0, 0, 11), // "JourneyList"
QT_MOC_LITERAL(1, 12, 7), // "MyRoles"
QT_MOC_LITERAL(2, 20, 13), // "StartStopRole"
QT_MOC_LITERAL(3, 34, 11), // "EndStopRole"
QT_MOC_LITERAL(4, 46, 13), // "StartTimeRole"
QT_MOC_LITERAL(5, 60, 11), // "EndTimeRole"
QT_MOC_LITERAL(6, 72, 10), // "LineNoRole"
QT_MOC_LITERAL(7, 83, 8) // "TypeRole"

    },
    "JourneyList\0MyRoles\0StartStopRole\0"
    "EndStopRole\0StartTimeRole\0EndTimeRole\0"
    "LineNoRole\0TypeRole"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_JourneyList[] = {

 // content:
       8,       // revision
       0,       // classname
       0,    0, // classinfo
       0,    0, // methods
       0,    0, // properties
       1,   14, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // enums: name, alias, flags, count, data
       1,    1, 0x0,    6,   19,

 // enum data: key, value
       2, uint(JourneyList::StartStopRole),
       3, uint(JourneyList::EndStopRole),
       4, uint(JourneyList::StartTimeRole),
       5, uint(JourneyList::EndTimeRole),
       6, uint(JourneyList::LineNoRole),
       7, uint(JourneyList::TypeRole),

       0        // eod
};

void JourneyList::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    Q_UNUSED(_o);
    Q_UNUSED(_id);
    Q_UNUSED(_c);
    Q_UNUSED(_a);
}

QT_INIT_METAOBJECT const QMetaObject JourneyList::staticMetaObject = { {
    QMetaObject::SuperData::link<QAbstractListModel::staticMetaObject>(),
    qt_meta_stringdata_JourneyList.data,
    qt_meta_data_JourneyList,
    qt_static_metacall,
    nullptr,
    nullptr
} };


const QMetaObject *JourneyList::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *JourneyList::qt_metacast(const char *_clname)
{
    if (!_clname) return nullptr;
    if (!strcmp(_clname, qt_meta_stringdata_JourneyList.stringdata0))
        return static_cast<void*>(this);
    return QAbstractListModel::qt_metacast(_clname);
}

int JourneyList::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QAbstractListModel::qt_metacall(_c, _id, _a);
    return _id;
}
QT_WARNING_POP
QT_END_MOC_NAMESPACE
