/****************************************************************************
** Meta object code from reading C++ file 'Network.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.15.2)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include <memory>
#include "../../Public_Transport/Network.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#include <QtCore/QList>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'Network.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.15.2. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
QT_WARNING_PUSH
QT_WARNING_DISABLE_DEPRECATED
struct qt_meta_stringdata_Network_t {
    QByteArrayData data[36];
    char stringdata0[452];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_Network_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_Network_t qt_meta_stringdata_Network = {
    {
QT_MOC_LITERAL(0, 0, 7), // "Network"
QT_MOC_LITERAL(1, 8, 14), // "qstops_changed"
QT_MOC_LITERAL(2, 23, 0), // ""
QT_MOC_LITERAL(3, 24, 12), // "qqst_changed"
QT_MOC_LITERAL(4, 37, 9), // "a_changed"
QT_MOC_LITERAL(5, 47, 20), // "qjourneys_no_changed"
QT_MOC_LITERAL(6, 68, 22), // "qqjourneyslist_changed"
QT_MOC_LITERAL(7, 91, 20), // "loader_state_changed"
QT_MOC_LITERAL(8, 112, 14), // "qlines_changed"
QT_MOC_LITERAL(9, 127, 22), // "routes_in_line_changed"
QT_MOC_LITERAL(10, 150, 18), // "qlinestops_changed"
QT_MOC_LITERAL(11, 169, 19), // "folder_name_changed"
QT_MOC_LITERAL(12, 189, 8), // "vyhledat"
QT_MOC_LITERAL(13, 198, 4), // "from"
QT_MOC_LITERAL(14, 203, 2), // "to"
QT_MOC_LITERAL(15, 206, 3), // "hod"
QT_MOC_LITERAL(16, 210, 3), // "min"
QT_MOC_LITERAL(17, 214, 21), // "save_journeis_to_file"
QT_MOC_LITERAL(18, 236, 6), // "source"
QT_MOC_LITERAL(19, 243, 4), // "load"
QT_MOC_LITERAL(20, 248, 9), // "filename2"
QT_MOC_LITERAL(21, 258, 17), // "create_qlinestops"
QT_MOC_LITERAL(22, 276, 11), // "line_number"
QT_MOC_LITERAL(23, 288, 15), // "set_folder_name"
QT_MOC_LITERAL(24, 304, 11), // "folder_name"
QT_MOC_LITERAL(25, 316, 12), // "qjourneys_no"
QT_MOC_LITERAL(26, 329, 10), // "QList<int>"
QT_MOC_LITERAL(27, 340, 14), // "qqjourneyslist"
QT_MOC_LITERAL(28, 355, 19), // "QList<JourneyList*>"
QT_MOC_LITERAL(29, 375, 6), // "qstops"
QT_MOC_LITERAL(30, 382, 4), // "qqst"
QT_MOC_LITERAL(31, 387, 12), // "loader_state"
QT_MOC_LITERAL(32, 400, 6), // "qlines"
QT_MOC_LITERAL(33, 407, 14), // "routes_in_line"
QT_MOC_LITERAL(34, 422, 18), // "QList<QStringList>"
QT_MOC_LITERAL(35, 441, 10) // "qlinestops"

    },
    "Network\0qstops_changed\0\0qqst_changed\0"
    "a_changed\0qjourneys_no_changed\0"
    "qqjourneyslist_changed\0loader_state_changed\0"
    "qlines_changed\0routes_in_line_changed\0"
    "qlinestops_changed\0folder_name_changed\0"
    "vyhledat\0from\0to\0hod\0min\0save_journeis_to_file\0"
    "source\0load\0filename2\0create_qlinestops\0"
    "line_number\0set_folder_name\0folder_name\0"
    "qjourneys_no\0QList<int>\0qqjourneyslist\0"
    "QList<JourneyList*>\0qstops\0qqst\0"
    "loader_state\0qlines\0routes_in_line\0"
    "QList<QStringList>\0qlinestops"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_Network[] = {

 // content:
       8,       // revision
       0,       // classname
       0,    0, // classinfo
      15,   14, // methods
       9,  126, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
      10,       // signalCount

 // signals: name, argc, parameters, tag, flags
       1,    0,   89,    2, 0x06 /* Public */,
       3,    0,   90,    2, 0x06 /* Public */,
       4,    0,   91,    2, 0x06 /* Public */,
       5,    0,   92,    2, 0x06 /* Public */,
       6,    0,   93,    2, 0x06 /* Public */,
       7,    0,   94,    2, 0x06 /* Public */,
       8,    0,   95,    2, 0x06 /* Public */,
       9,    0,   96,    2, 0x06 /* Public */,
      10,    0,   97,    2, 0x06 /* Public */,
      11,    0,   98,    2, 0x06 /* Public */,

 // methods: name, argc, parameters, tag, flags
      12,    4,   99,    2, 0x02 /* Public */,
      17,    5,  108,    2, 0x02 /* Public */,
      19,    1,  119,    2, 0x02 /* Public */,
      21,    1,  122,    2, 0x02 /* Public */,
      23,    0,  125,    2, 0x02 /* Public */,

 // signals: parameters
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,

 // methods: parameters
    QMetaType::Void, QMetaType::Int, QMetaType::Int, QMetaType::Int, QMetaType::Int,   13,   14,   15,   16,
    QMetaType::Void, QMetaType::QUrl, QMetaType::QString, QMetaType::QString, QMetaType::Int, QMetaType::Int,   18,   13,   14,   15,   16,
    QMetaType::Void, QMetaType::QString,   20,
    QMetaType::Void, QMetaType::Int,   22,
    QMetaType::Void,

 // properties: name, type, flags
      24, QMetaType::QString, 0x00495001,
      25, 0x80000000 | 26, 0x00495009,
      27, 0x80000000 | 28, 0x00495009,
      29, QMetaType::QStringList, 0x00495001,
      30, QMetaType::QStringList, 0x00495001,
      31, QMetaType::Float, 0x00495003,
      32, QMetaType::QStringList, 0x00495001,
      33, 0x80000000 | 34, 0x00495009,
      35, QMetaType::QStringList, 0x00495001,

 // properties: notify_signal_id
       9,
       3,
       4,
       0,
       1,
       5,
       6,
       7,
       8,

       0        // eod
};

void Network::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        auto *_t = static_cast<Network *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->qstops_changed(); break;
        case 1: _t->qqst_changed(); break;
        case 2: _t->a_changed(); break;
        case 3: _t->qjourneys_no_changed(); break;
        case 4: _t->qqjourneyslist_changed(); break;
        case 5: _t->loader_state_changed(); break;
        case 6: _t->qlines_changed(); break;
        case 7: _t->routes_in_line_changed(); break;
        case 8: _t->qlinestops_changed(); break;
        case 9: _t->folder_name_changed(); break;
        case 10: _t->vyhledat((*reinterpret_cast< int(*)>(_a[1])),(*reinterpret_cast< int(*)>(_a[2])),(*reinterpret_cast< int(*)>(_a[3])),(*reinterpret_cast< int(*)>(_a[4]))); break;
        case 11: _t->save_journeis_to_file((*reinterpret_cast< QUrl(*)>(_a[1])),(*reinterpret_cast< const QString(*)>(_a[2])),(*reinterpret_cast< const QString(*)>(_a[3])),(*reinterpret_cast< int(*)>(_a[4])),(*reinterpret_cast< int(*)>(_a[5]))); break;
        case 12: _t->load((*reinterpret_cast< QString(*)>(_a[1]))); break;
        case 13: _t->create_qlinestops((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 14: _t->set_folder_name(); break;
        default: ;
        }
    } else if (_c == QMetaObject::IndexOfMethod) {
        int *result = reinterpret_cast<int *>(_a[0]);
        {
            using _t = void (Network::*)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&Network::qstops_changed)) {
                *result = 0;
                return;
            }
        }
        {
            using _t = void (Network::*)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&Network::qqst_changed)) {
                *result = 1;
                return;
            }
        }
        {
            using _t = void (Network::*)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&Network::a_changed)) {
                *result = 2;
                return;
            }
        }
        {
            using _t = void (Network::*)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&Network::qjourneys_no_changed)) {
                *result = 3;
                return;
            }
        }
        {
            using _t = void (Network::*)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&Network::qqjourneyslist_changed)) {
                *result = 4;
                return;
            }
        }
        {
            using _t = void (Network::*)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&Network::loader_state_changed)) {
                *result = 5;
                return;
            }
        }
        {
            using _t = void (Network::*)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&Network::qlines_changed)) {
                *result = 6;
                return;
            }
        }
        {
            using _t = void (Network::*)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&Network::routes_in_line_changed)) {
                *result = 7;
                return;
            }
        }
        {
            using _t = void (Network::*)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&Network::qlinestops_changed)) {
                *result = 8;
                return;
            }
        }
        {
            using _t = void (Network::*)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&Network::folder_name_changed)) {
                *result = 9;
                return;
            }
        }
    } else if (_c == QMetaObject::RegisterPropertyMetaType) {
        switch (_id) {
        default: *reinterpret_cast<int*>(_a[0]) = -1; break;
        case 2:
            *reinterpret_cast<int*>(_a[0]) = qRegisterMetaType< QList<JourneyList*> >(); break;
        case 7:
            *reinterpret_cast<int*>(_a[0]) = qRegisterMetaType< QList<QStringList> >(); break;
        case 1:
            *reinterpret_cast<int*>(_a[0]) = qRegisterMetaType< QList<int> >(); break;
        }
    }

#ifndef QT_NO_PROPERTIES
    else if (_c == QMetaObject::ReadProperty) {
        auto *_t = static_cast<Network *>(_o);
        Q_UNUSED(_t)
        void *_v = _a[0];
        switch (_id) {
        case 0: *reinterpret_cast< QString*>(_v) = _t->get_folder_name(); break;
        case 1: *reinterpret_cast< QList<int>*>(_v) = _t->get_qjourneys_no(); break;
        case 2: *reinterpret_cast< QList<JourneyList*>*>(_v) = _t->get_qqjourneyslist(); break;
        case 3: *reinterpret_cast< QStringList*>(_v) = _t->get_qstops(); break;
        case 4: *reinterpret_cast< QStringList*>(_v) = _t->get_qqst(); break;
        case 5: *reinterpret_cast< float*>(_v) = _t->get_loader_state(); break;
        case 6: *reinterpret_cast< QStringList*>(_v) = _t->get_qlines(); break;
        case 7: *reinterpret_cast< QList<QStringList>*>(_v) = _t->get_routes_in_line(); break;
        case 8: *reinterpret_cast< QStringList*>(_v) = _t->get_qlinestops(); break;
        default: break;
        }
    } else if (_c == QMetaObject::WriteProperty) {
        auto *_t = static_cast<Network *>(_o);
        Q_UNUSED(_t)
        void *_v = _a[0];
        switch (_id) {
        case 5: _t->set_loader_state(*reinterpret_cast< float*>(_v)); break;
        default: break;
        }
    } else if (_c == QMetaObject::ResetProperty) {
    }
#endif // QT_NO_PROPERTIES
}

QT_INIT_METAOBJECT const QMetaObject Network::staticMetaObject = { {
    QMetaObject::SuperData::link<QObject::staticMetaObject>(),
    qt_meta_stringdata_Network.data,
    qt_meta_data_Network,
    qt_static_metacall,
    nullptr,
    nullptr
} };


const QMetaObject *Network::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *Network::qt_metacast(const char *_clname)
{
    if (!_clname) return nullptr;
    if (!strcmp(_clname, qt_meta_stringdata_Network.stringdata0))
        return static_cast<void*>(this);
    return QObject::qt_metacast(_clname);
}

int Network::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QObject::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 15)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 15;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 15)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 15;
    }
#ifndef QT_NO_PROPERTIES
    else if (_c == QMetaObject::ReadProperty || _c == QMetaObject::WriteProperty
            || _c == QMetaObject::ResetProperty || _c == QMetaObject::RegisterPropertyMetaType) {
        qt_static_metacall(this, _c, _id, _a);
        _id -= 9;
    } else if (_c == QMetaObject::QueryPropertyDesignable) {
        _id -= 9;
    } else if (_c == QMetaObject::QueryPropertyScriptable) {
        _id -= 9;
    } else if (_c == QMetaObject::QueryPropertyStored) {
        _id -= 9;
    } else if (_c == QMetaObject::QueryPropertyEditable) {
        _id -= 9;
    } else if (_c == QMetaObject::QueryPropertyUser) {
        _id -= 9;
    }
#endif // QT_NO_PROPERTIES
    return _id;
}

// SIGNAL 0
void Network::qstops_changed()
{
    QMetaObject::activate(this, &staticMetaObject, 0, nullptr);
}

// SIGNAL 1
void Network::qqst_changed()
{
    QMetaObject::activate(this, &staticMetaObject, 1, nullptr);
}

// SIGNAL 2
void Network::a_changed()
{
    QMetaObject::activate(this, &staticMetaObject, 2, nullptr);
}

// SIGNAL 3
void Network::qjourneys_no_changed()
{
    QMetaObject::activate(this, &staticMetaObject, 3, nullptr);
}

// SIGNAL 4
void Network::qqjourneyslist_changed()
{
    QMetaObject::activate(this, &staticMetaObject, 4, nullptr);
}

// SIGNAL 5
void Network::loader_state_changed()
{
    QMetaObject::activate(this, &staticMetaObject, 5, nullptr);
}

// SIGNAL 6
void Network::qlines_changed()
{
    QMetaObject::activate(this, &staticMetaObject, 6, nullptr);
}

// SIGNAL 7
void Network::routes_in_line_changed()
{
    QMetaObject::activate(this, &staticMetaObject, 7, nullptr);
}

// SIGNAL 8
void Network::qlinestops_changed()
{
    QMetaObject::activate(this, &staticMetaObject, 8, nullptr);
}

// SIGNAL 9
void Network::folder_name_changed()
{
    QMetaObject::activate(this, &staticMetaObject, 9, nullptr);
}
QT_WARNING_POP
QT_END_MOC_NAMESPACE
